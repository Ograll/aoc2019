// day3.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#pragma once

#include <unordered_set>
#include <unordered_map>
#include <queue>

struct GraphHasEdgeValue {};
struct GraphHasNoEdgeValue {};


template <class NodeType, class EdgeType = void>
struct Graph
{
public:
	using node_type = NodeType;
	using edge_type = EdgeType;
	using edge_valued = GraphHasEdgeValue;

	void add(const node_type& a, const node_type& b, const edge_type& e)
	{
		_nodes.emplace(a);
		_nodes.emplace(b);
		_children[a].emplace(b, e);
		_children[b].emplace(a, e);
	}

	const std::unordered_set<node_type>& nodes() const
	{
		return _nodes;
	}

	const std::unordered_map<node_type, edge_type>& children(const node_type& a) const
	{
		return _children.find(a)->second;
	}

private:
	std::unordered_set<node_type> _nodes;
	std::unordered_map<node_type, std::unordered_map<node_type, edge_type>> _children;
};

template <class NodeType>
struct Graph<NodeType, void>
{
public:
	using node_type = NodeType;
	using edge_type = void;
	using edge_valued = GraphHasNoEdgeValue;

	void add(const node_type& a, const node_type& b)
	{
		_nodes.emplace(a);
		_nodes.emplace(b);
		_children[a].emplace(b);
		_children[b].emplace(a);
	}

	const std::unordered_set<node_type>& nodes() const
	{
		return _nodes;
	}

	const std::unordered_set<node_type>& children(const node_type& a) const
	{
		return _children.find(a)->second;
	}

private:
	std::unordered_set<node_type> _nodes;
	std::unordered_map<node_type, std::unordered_set<node_type>> _children;
};

template <class NodeType, class EdgeType = void>
struct DirectedGraph
{
public:
	using node_type = NodeType;
	using edge_type = EdgeType;
	using edge_valued = GraphHasEdgeValue;

	void add(const node_type& a, const node_type& b, const edge_type& e)
	{
		_nodes.emplace(a);
		_nodes.emplace(b);
		_children[a].emplace(b, e);
	}

	const std::unordered_map<node_type, edge_type>& children(const node_type& a) const
	{
		auto itor = _children.find(a);
		if (itor == _children.end())
		{
			return _empty;
		}
		return itor->second;
	}

	const std::unordered_set<node_type>& nodes() const
	{
		return _nodes;
	}

	DirectedGraph reverse() const
	{
		DirectedGraph reversed;

		for (auto a : _children)
		{
			for (auto b : a.second)
			{
				reversed.add(b.first, a.first, b.second);
			}
		}
		return reversed;
	}
private:
	std::unordered_set<node_type> _nodes;
	std::unordered_map<node_type, std::unordered_map<node_type, edge_type>> _children;
	std::unordered_map<node_type, edge_type> _empty;
};

template <class NodeType>
struct DirectedGraph<NodeType, void>
{
public:
	using node_type = NodeType;
	using edge_type = void;
	using edge_valued = GraphHasNoEdgeValue;

	void add(const node_type& a, const node_type& b)
	{
		_nodes.emplace(a);
		_nodes.emplace(b);
		_children[a].emplace(b);
	}

	const std::unordered_set<node_type>& children(const node_type& a) const
	{
		auto itor = _children.find(a);
		if (itor == _children.end())
		{
			return _empty;
		}
		return itor->second;
	}

	const std::unordered_set<node_type>& nodes() const
	{
		return _nodes;
	}

	DirectedGraph reverse() const
	{
		DirectedGraph reversed;

		for (auto a : _children)
		{
			for (auto b : a.second)
			{
				reversed.add(b, a.first);
			}
		}
		return reversed;
	}
private:
	std::unordered_set<node_type> _nodes;
	std::unordered_map<node_type, std::unordered_set<node_type>> _children;
	std::unordered_set<node_type> _empty;
};

template <typename _Graph, typename previsit, typename postvisit>
struct explorerer
{
	std::unordered_set<typename _Graph::node_type> visited;
	const _Graph& g;
	previsit& previs;
	postvisit& postvis;

	explorerer(const _Graph& g, previsit& previs, postvisit& postvis) :g(g), previs(previs), postvis(postvis)
	{
	}
	void explore(const typename _Graph::node_type& node)
	{
		if (visited.insert(node).second == false)
			return;
		previs(node);
		for (auto child : g.children(node))
		{
			explore(child);
		}
		postvis(node);
	}
};

template <typename _Graph>
auto reachability(const _Graph& g) -> std::unordered_map<typename _Graph::node_type, int>
{
	struct Reacher
	{
		Reacher(const _Graph& graph) : _graph(graph) {}

		std::unordered_set<typename _Graph::node_type> _visited;

		int explore(const typename _Graph::node_type &node)
		{
			exploreR(node);
			return (int)_visited.size();
		}

	private:
		void exploreR(const typename _Graph::node_type& node)
		{
			if (_visited.emplace(node).second == false)
			{
				// already visited.
				return;
			}

			// Loop through all the child
			for (const auto& child : _graph.children(node))
			{
				exploreR(child);
			}
		}

		const _Graph& _graph;
	};
	std::unordered_map<typename _Graph::node_type, int> result;

	for (auto node : g.nodes())
	{
		Reacher reacher(g);
		result[node] = reacher.explore(node);
	}

	return result;
}

template <typename _Graph, typename previsit, typename postvisit>
void dfs(const _Graph& g, previsit previs, postvisit postvis)
{
	explorerer a(g, previs, postvis);
	for (auto n : g.nodes())
	{
		a.explore(n);
	}
}

template <typename _Graph, typename previsit, typename postvisit>
void explore(const _Graph& g, const typename _Graph::node_type& start, previsit previs, postvisit postvis)
{
	explorerer a(g, previs, postvis);
	a.explore(start);
}

template <class _Graph>
std::unordered_map<typename _Graph::node_type, int> _bfs(const _Graph& g, const typename _Graph::node_type& start, GraphHasNoEdgeValue)
{
	using node_type = typename _Graph::node_type;
	using edge_type = int;
	using QueueEntry = std::pair<node_type, edge_type>;
	struct Comparer
	{
		bool operator()(const QueueEntry& a, QueueEntry& b) const
		{
			return a.second > b.second;
		}
	};
	using queue = std::priority_queue<QueueEntry, std::vector<QueueEntry>, Comparer>;
	queue frontier;
	frontier.emplace(start, edge_type{});

	std::unordered_map<node_type, edge_type> explored;

	while (frontier.empty() == false)
	{
		auto item = frontier.top();
		frontier.pop();

		if (explored.find(item.first) != explored.end())
		{
			continue;
		}
		explored[item.first] = item.second;

		for (auto& child : g.children(item.first))
		{
			frontier.emplace(child, item.second + 1);
		}
	}
	return explored;
}

template <class _Graph>
std::unordered_map<typename _Graph::node_type, typename _Graph::edge_type> _bfs(const _Graph& g, const typename _Graph::node_type& start, GraphHasEdgeValue)
{
	using node_type = typename _Graph::node_type;
	using edge_type = typename _Graph::edge_type;
	using QueueEntry = std::pair<node_type, edge_type>;
	struct Comparer
	{
		bool operator()(const QueueEntry& a, QueueEntry& b) const
		{
			return a.second > b.second;
		}
	};
	using queue = std::priority_queue<QueueEntry, std::vector<QueueEntry>, Comparer>;
	queue frontier;
	frontier.emplace(start, edge_type{});

	std::unordered_map<typename _Graph::node_type, typename _Graph::edge_type> explored;

	while (frontier.empty() == false)
	{
		auto item = frontier.top();
		frontier.pop();

		if (explored.find(item.first) != explored.end())
		{
			continue;
		}
		explored[item.first] = item.second;

		for (auto& child : g.children(item.first))
		{
			frontier.emplace(child.first, item.second + child.second);
		}
	}
	return explored;
}

template <class _Graph>
auto bfs(const _Graph& g, const typename _Graph::node_type& start) ->
decltype(_bfs(g, start, typename _Graph::edge_valued()))
{
	return _bfs(g, start, typename _Graph::edge_valued());
}