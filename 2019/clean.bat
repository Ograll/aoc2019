@echo off
call :clean day1
call :clean day2
call :clean day3
call :clean day4
call :clean day5
call :clean day6
call :clean day7
call :clean day8
call :clean day9
call :clean day10
call :clean day11

goto :EOF

:clean
rmdir /s /q %1\.vs 2> nul
rmdir /s /q %1\_build 2> nul
del %1\%1.vcxproj.user 2> nul