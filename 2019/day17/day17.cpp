// day17.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <algorithm>
#include <iostream>
#include <string>
#include <vector>
#include <charconv>
#include <cassert>
#include <regex>
#include <fstream>
#include <map>
#include <unordered_map>
#include <thread>
#include <set>

#include "../../tools.h"
#include "../../graph.h"

using namespace std;

std::vector<int64_t> getCodes(const char* program)
{
	auto scodes = split(program, ",");

	std::vector<int64_t>codes;
	for (auto scode : scodes)
	{
		int64_t v;
		std::from_chars(scode.data(), scode.data() + scode.size(), v);
		codes.emplace_back(v);
	}
	return codes;
}

struct Computer
{
	Computer(const char* program)
	{
		_codes = getCodes(program);
	}

	Computer(const std::vector<int64_t>& program)
	{
		_codes = program;
	}

	void addInputs(const vector<int>& inputs)
	{
		_inputs.insert(_inputs.end(), inputs.begin(), inputs.end());
	}

	bool hasOutputs()
	{
		return _outputs.size();
	}

	bool getOutput(int64_t& x)
	{
		if (_outputs.empty())
			return false;
		x = *_outputs.begin();
		_outputs.erase(_outputs.begin());
		return true;
	}

	bool halted()
	{
		return _halted;
	}

	void executeUntilOutOfInputs()
	{
		auto inputIt = _inputs.begin();
		for (;;)
		{
			_instructionsRan++;
			int64_t instrution = _codes[_offset];

			int64_t modes[3] = { (instrution / 100) % 10,
				 (instrution / 1000) % 10,
				(instrution / 10000) % 10 };
			instrution %= 100;

			auto readMode = [&](int off)
			{
				if (modes[off] == 1)
				{
					return  read(_offset + off + 1);
				}
				else if (modes[off] == 2)
				{
					return read(read(_offset + off + 1) + _relBase);
				}
				else
				{
					return read(read(_offset + off + 1));
				}
			};
			auto writeMode = [&](int off, int64_t value)
			{
				if (modes[off] == 1)
				{
					assert(0);
				}
				else if (modes[off] == 2)
				{
					return write(read(_offset + off + 1) + _relBase, value);
				}
				else
				{
					return write(read(_offset + off + 1), value);
				}
			};

			switch (instrution)
			{
			case 1:
				writeMode(2, readMode(0) + readMode(1));
				_offset += 4;
				break;
			case 2:
				writeMode(2, readMode(0) * readMode(1));
				_offset += 4;
				break;
			case 3:
			{
				if (_inputs.size())
				{
					writeMode(0, _inputs[0]);
					_inputs.erase(_inputs.begin());
					_offset += 2;
				}
				else
				{
					return;
				}
			}
			break;
			case 4:
				_outputs.emplace_back(readMode(0));
				_offset += 2;
				break;
			case 5:
				if (readMode(0) != 0)
				{
					_offset = readMode(1);
				}
				else
				{
					_offset += 3;
				}
				break;
			case 6:
				if (readMode(0) == 0)
				{
					_offset = readMode(1);
				}
				else
				{
					_offset += 3;
				}
				break;
			case 7:
				writeMode(2, (readMode(0) < readMode(1)) ? 1 : 0);
				_offset += 4;
				break;
			case 8:
				writeMode(2, (readMode(0) == readMode(1)) ? 1 : 0);
				_offset += 4;
				break;
			case 9:
				_relBase += readMode(0);
				_offset += 2;
				break;
			case 99:
				_halted = true;
				return;
			default:
				assert(0);
			}
		}
		assert(0);
	}
private:
	int64_t read(int64_t at)
	{
		expand(at);
		return _codes[at];
	}
	void write(int64_t at, int64_t v)
	{
		expand(at);
		_codes[at] = v;
	}
	void expand(int64_t at)
	{
		assert(at >= 0);
		if ((size_t)at >= _codes.size())
		{
			_codes.resize(at + 1);
		}
	}

	int64_t _instructionsRan = 0;
	int64_t _relBase = 0;
	std::vector<int64_t> _codes;
	size_t _offset = 0;
	bool _halted = false;
	std::vector<int64_t> _inputs;				// Current inputs.
	std::vector<int64_t> _outputs;
};

void inputToDirection(int dir, int& dx, int& dy)
{
	switch (dir)
	{
	case 1: dx = 0; dy = 1; return;
	case 2: dx = 0; dy = -1; return;
	case 3: dx = 1; dy = 0; return;
	case 4: dx = -1; dy = 0; return;
	}
}

// BFS on the intcode machine.
std::map<pair<int, int>, int> exploreWorld(const std::vector<int64_t>& code)
{
	struct State
	{
		std::shared_ptr<Computer> computer;	// The computer state before this action was taken.
		int move;							// The action to take.
		int moves;							// The number of actions taken.
		int x = 0, y = 0;					// The world position.
	};
	using QueueEntry = State;
	struct Comparer
	{
		bool operator()(const QueueEntry& a, QueueEntry& b) const
		{
			return a.moves > b.moves;
		}
	};
	using queue = std::priority_queue<QueueEntry, std::vector<QueueEntry>, Comparer>;
	queue frontier;
	frontier.emplace(State{ std::make_shared<Computer>(code), 0, 0, 0, 0 });

	std::map<pair<int, int>, int> explored;

	while (frontier.empty() == false)
	{
		QueueEntry item = frontier.top();
		frontier.pop();

		if (explored.find(pair(item.x, item.y)) != explored.end())
		{
			continue;
		}
		shared_ptr<Computer> program = item.computer;
		if (item.moves == 0)
		{
			explored[pair(item.x, item.y)] = 1;
		}
		else
		{
			program = shared_ptr<Computer>(new Computer(*program));

			program->addInputs({ item.move });
			program->executeUntilOutOfInputs();
			int64_t x;
			program->getOutput(x);
			assert(program->hasOutputs() == false);
			explored[pair(item.x, item.y)] = (int)x;
			if (x == 0)
			{
				continue;
			}
		}

		for (auto& input : { 1, 2, 3, 4 })
		{
			int dx, dy;
			inputToDirection(input, dx, dy);
			frontier.emplace(State{ program, input, item.moves + 1, item.x + dx, item.y + dy });
		}
	}
	return explored;
}

pair<vector<vector<pair<char, int>>>, vector<int>> tryCompress(const std::vector<pair<char, int>> &commands)
{
	// Now that we have the commands lets try and compress them by having the blocks. We will keep track of runs and keep the best three.

	// We first try and compress A to varying lengths. Then we try to repeat it as many tiimes as possible, then
	// we do b, then we repeat a & b as many times as possible, then we do C
	pair<int, int> A;
	pair<int, int> B;
	pair<int, int> C;
	vector<int> uses;
	static const int maxLen = 20;
	auto runMatches = [&commands](pair<int, int>& v, size_t at)
	{
		for (size_t i = 0; i < v.second - v.first; i++)
		{
			if (commands[v.first + i].first  != commands[at + i].first ||
				commands[v.first + i].second != commands[at + i].second)
				return false;
		}
		return true;
	};
	auto setExceeds = [&commands](pair<int, int>& v)
	{
		// Build it it is the best way
		string s;

		for (size_t i = v.first; i < v.second; i++)
		{
			if (!s.empty()) s += ",";
			s += commands[i].first;
			s += ",";
			s += std::to_string(commands[i].second);
		}
		return (s.size() > maxLen);
	};
	auto usesExceds = [&commands](const std::vector<int>& uses)
	{
		string s;

		for(auto i : uses)
		{
			if (!s.empty()) s += ",";
			s += i + 'A';
		}
		return (s.size() > maxLen);
	};

	for(size_t ea = 1; ea < commands.size(); ea++)
	{
		A = pair<int, int>(0, (int)ea );
		if (setExceeds(A))
			continue;
		uses.clear();
		uses.emplace_back(0);
		// Repeat A if possible
		size_t at = ea;
		while (at + A.second - A.first <= commands.size() && runMatches(A, at))
		{
			at += A.second - A.first;
			uses.emplace_back(0);
		}

		// Repeat now b as much as possible.

		auto sb = at;
		auto usesA = uses;
		for (size_t eb = sb + 1; eb < commands.size(); eb++)
		{
			B = pair<int, int>((int)sb, (int)eb );
			if (setExceeds(B))
				continue;
			uses = usesA;
			uses.emplace_back(1);

			// Repeat A & B if possible
			size_t at = eb;
			for (;;)
			{
				if (at + A.second - A.first <= commands.size() && runMatches(A, at))
				{
					at += A.second - A.first;
					uses.emplace_back(0);
				}
				else if (at + B.second - B.first <= commands.size() && runMatches(B, at))
				{
					at += B.second - B.first;
					uses.emplace_back(1);
				}
				else
				{
					break;
				}
			}

			auto sc = at;
			auto usesB = uses;
			for (size_t ec = sc + 1; ec < commands.size(); ec++)
			{
				C = pair<int, int>((int)sc, (int)ec );
				if (setExceeds(C))
					continue;
				uses = usesB;
				uses.emplace_back(2);

				// Repeat A & B if possible
				size_t at = ec;
				for (;;)
				{
					if (usesExceds(uses))
						break;
					if (at == commands.size())
					{
						pair<vector< std::vector<pair<char, int>>>, vector<int>> out;
						out.first.resize(3);
						for (int i = A.first; i < A.second; i++)
							out.first[0].emplace_back(commands[i]);
						for (int i = B.first; i < B.second; i++)
							out.first[1].emplace_back(commands[i]);
						for (int i = C.first; i < C.second; i++)
							out.first[2].emplace_back(commands[i]);
						out.second = uses;
						return out;
					}
					if (at + A.second - A.first <= commands.size() && runMatches(A, at))
					{
						at += A.second - A.first;
						uses.emplace_back(0);
					}
					else if (at + B.second - B.first <= commands.size() && runMatches(B, at))
					{
						at += B.second - B.first;
						uses.emplace_back(1);
					}
					else if (at + C.second -C.first <= commands.size() && runMatches(C, at))
					{
						at += C.second - C.first;
						uses.emplace_back(2);
					}
					else
					{
						break;
					}
				}
			}
		}
	}
	return pair<vector< std::vector<pair<char, int>>>, vector<int>>();
}

static const pair<int, int> directions[] =
{
	{ 0, -1 },
	{ 1, 0},
	{ 0, 1},
	{-1, 0}
};

std::map<pair<int, int>, char> runPart1(const char* rawCode, int &sx, int &sy)
{
	auto program = getCodes(rawCode);

	Computer c(program);

	c.executeUntilOutOfInputs();
	int sum = 0;
	int64_t o;
	string s;

	int x = 0;
	int y = 0;
	std::map<pair<int, int>, char> map;
	while (c.getOutput(o))
	{
		if ((char)o == '\n')
		{
			y++;
			x = 0;
		}
		else
		{
			map[pair(x, y)] = (char)o;
			x++;
		}
		sx = max(x, sx);
		sy = max(y, sy);
		s += (char)o;
	}
	return map;
}

int64_t runPart2(const char* rawCode, const char* input, int& sx, int& sy)
{
	auto program = getCodes(rawCode);
	program[0] = 2;

	Computer c(program);

	if (input)
	{
		while (*input)
		{
			c.addInputs({ *input });
			++input;
		}
	}

	c.executeUntilOutOfInputs();

	string s;
	int64_t x;
	while (c.getOutput(x));
	return x;
}

int main()
{
	string input = readFile("problem.txt");
	int sx, sy;
	auto map = runPart1(input.c_str(), sx, sy);

	// Forall the stuff

	 int sum = 0;
	 std::map<pair<int, int>, char> ats;
	 for (auto x : map)
	{
		// If there is a hahs in all directions in it an 
		 ats[x.first] = x.second;

		 if (x.second != '#')
			 continue;

		auto above = map.find(pair(x.first.first, x.first.second - 1));
		auto below = map.find(pair(x.first.first, x.first.second + 1));
		auto left = map.find(pair(x.first.first -1, x.first.second));
		auto right = map.find(pair(x.first.first+1, x.first.second));
		if (above != map.end() && below != map.end() && left != map.end() && right != map.end())
		{
			if (above->second == '#' && below->second == '#' && left->second == '#' && right->second == '#')
			{
				ats[x.first] = 'O';
				int alignment = x.first.first * x.first.second;
				sum += alignment;
			}
		}
	}
	printf("Part 1: %d\n", sum);

	 {
		 // Find the robot
		 int cells = 0;
		 int x, y;
		 for (auto c : map)
		 {
			 if (c.second == '^')
			 {
				 x = c.first.first;
				 y = c.first.second;
				 c.second = '#';
			 }
			 if (c.second == '#')
			 {
				 cells++;
			 }
		 }
		 auto onThePath = [&map, &x, &y](int direction, int delta)
		 {
			 auto dir = directions[direction];
			 auto itor = map.find(pair(x + dir.first * delta, y + dir.second * delta));
			 if (itor == map.end()) return false;
			 return itor->second == '#';
		 };
		 auto move = [&x, &y](int direction, int delta)
		 {
			 auto dir = directions[direction];
			 x = x + dir.first * delta;
			 y = y + dir.second * delta;
		 };

		 int direction = 1;
		 std::vector<pair<char, int>> pathTaken{ { 'R', 0 } };
		 std::set<pair<int, int>> visisted;
		 pair<vector<vector<pair<char, int>>>, vector<int>> solution;
		 for (;;)
		 {
			 visisted.emplace(pair(x, y));
			 if (visisted.size() == cells)
			 {
				 solution = tryCompress(pathTaken);
				 break;
			 }
			 int tryLeft = ((direction - 1) + 4) % 4;
			 int tryRight = (direction + 1) % 4;
			 if (onThePath(direction, 1))
			 {
				 move(direction, 1);
				 pathTaken.back().second++;
			 }
			 else if (onThePath(tryRight, 1))
			 {
				 direction = tryRight;
				 pathTaken.emplace_back('R', 0);
			 }
			 else if (onThePath(tryLeft, 1))
			 {
				 direction = tryLeft;
				 pathTaken.emplace_back('L', 0);
			 }
			 else
			 {
				 break;
			 }
		 }

		 // Make a string out of the solution
		 string line1;
		 string programs[3];

		 
		 for (auto a : solution.second)
		 {
			 if(line1.empty() == false) line1.append(",");
			 line1 += a + 'A';
		 }
		 for (int i = 0; i < 3; i++)
		 {
			 for (auto p : solution.first[i])
			 {
				 if (programs[i].empty() == false) programs[i].append(",");
				 programs[i] += p.first;
				 programs[i] += ",";
				 programs[i] += std::to_string(p.second);
			 }
		 }

		 string finalSolution = line1 + "\n" + programs[0] + "\n" + programs[1] + "\n" + programs[2] + "\nn\n";

		 auto part2 = runPart2(input.c_str(), finalSolution.c_str(), sx, sy);
		 printf("Part 2: %lld\n", part2);
	 }
}