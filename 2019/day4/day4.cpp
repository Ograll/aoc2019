// day4.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>
#include <vector>
#include <charconv>
#include <cassert>
#include <regex>
#include <queue>
#include <set>
#include <map>
#include <algorithm>
#include <iterator>
#include <memory>
#include <functional>

#include "../../tools.h"

using namespace std;

bool partA(int i)
{
	int digits[6];
	digits[0] = (i / 100000) % 10;
	digits[1] = (i / 10000) % 10;
	digits[2] = (i / 1000) % 10;
	digits[3] = (i / 100) % 10;
	digits[4] = (i / 10) % 10;
	digits[5] = (i / 1) % 10;

	bool good = true;
	int repeats = 0;
	for (int j = 1; j < 6; j++)
	{
		if (digits[j - 1] > digits[j])
			good = false;
		if (digits[j - 1] == digits[j])
			repeats++;
	}
	if (repeats == 0)
		return false;
	if (good == false)
		return false;
	return true;
}

int partA(int start, int end)
{
	int k = 0;
	for (int i = start; i <= end; i++)
	{
		if (partA(i))
		{
			k++;
		}
	}
	return k;
}

bool partB(int i)
{
	int digits[6];

	digits[0] = (i / 100000) % 10;
	digits[1] = (i / 10000) % 10;
	digits[2] = (i / 1000) % 10;
	digits[3] = (i / 100) % 10;
	digits[4] = (i / 10) % 10;
	digits[5] = (i / 1) % 10;

	bool good = true;
	int repeats = 0;
	for (int j = 1; j < 6; j++)
	{
		if (digits[j - 1] > digits[j])
			good = false;
	}
	bool hasDouble = false;
	for (int j = 0; j < 6;)
	{
		int l;
		for (l = j + 1; l < 6; l++)
		{
			if (digits[j] != digits[l])
				break;
		}
		int len = l - j;
		if (len == 2)
		{
			hasDouble = true;
		}
		j = l;
	}
	if (good == false || hasDouble == false)
	{
		return false;
	}
	return true;
}

int partB(int start, int end)
{
	int k = 0;
	for (int i = start; i <= end; i++)
	{
		if(partB(i))
			k++;
	}
	return k;
}

int main()
{
	auto input = readFile("problem.txt");
	auto startEnd = split(input, "-");
	int start, end;
	from_chars(startEnd[0].data(), startEnd[0].data() + startEnd[0].size(), start);
	from_chars(startEnd[1].data(), startEnd[1].data() + startEnd[1].size(), end);

	printf("Example 1b: %d\n", partA(111111) ? 1 : 0);
	printf("Example 2b: %d\n", partA(223450) ? 1 : 0);
	printf("Example 3b: %d\n", partA(123789) ? 1 : 0);
	printf("Part a: %d\n", partA(start, end));

	printf("Example 1b: %d\n", partB(112233)? 1:0);
	printf("Example 2b: %d\n", partB(123444) ? 1 : 0);
	printf("Example 3b: %d\n", partB(111122) ? 1 : 0);
	printf("Part b: %d\n", partB(start, end));
}