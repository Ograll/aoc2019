// day11.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <algorithm>
#include <iostream>
#include <string>
#include <vector>
#include <charconv>
#include <cassert>
#include <regex>
#include <fstream>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <numeric>

#include "../../tools.h"

const char* example1 = R"(<x=-1, y=0, z=2>
<x=2, y=-10, z=-7>
<x=4, y=-8, z=8>
<x=3, y=5, z=-1>)";
const char* example2 = R"(<x=-8, y=-10, z=0>
<x=5, y=5, z=10>
<x=2, y=-7, z=3>
<x=9, y=-8, z=-3>)";

using namespace std;

struct Moon
{
	int x, y, z;
	int vx, vy, vz;
};

Moon createMoon(const string_view& sv)
{
	regex r(R"(\<x=(-?\d+), y=(-?\d+), z=(-?\d+)\>)");

	cmatch m;
	regex_search(sv.data(), sv.data() + sv.size(), m, r);

	Moon moon{};
	std::from_chars(sv.data() + m.position(1), sv.data() + m.position(1) + m.length(1), moon.x);
	std::from_chars(sv.data() + m.position(2), sv.data() + m.position(2) + m.length(2), moon.y);
	std::from_chars(sv.data() + m.position(3), sv.data() + m.position(3) + m.length(3), moon.z);
	return moon;
}

std::vector<Moon> createMoons(const std::vector<string_view>& inputs)
{
	vector<Moon> moons;
	for (auto& input : inputs)
	{
		moons.emplace_back(createMoon(input));
	}
	return moons;
}

std::vector<Moon> step(const std::vector<Moon>& moonsIn)
{
	auto moonsOut = moonsIn;
	// Apply forces to all moons
	for (size_t i = 0; i < moonsIn.size(); i++)
	{
		for (size_t j = 0; j < moonsIn.size(); j++)
		{
			if (i == j) continue;
			moonsOut[i].vx += (moonsIn[i].x == moonsIn[j].x) ? 0 :((moonsIn[i].x > moonsIn[j].x) ? -1 : 1);
			moonsOut[i].vy += (moonsIn[i].y == moonsIn[j].y) ? 0 :((moonsIn[i].y > moonsIn[j].y) ? -1 : 1);
			moonsOut[i].vz += (moonsIn[i].z == moonsIn[j].z) ? 0 :((moonsIn[i].z > moonsIn[j].z) ? -1 : 1);
		}
	}
	for (size_t i = 0; i < moonsIn.size(); i++)
	{
		moonsOut[i].x += moonsOut[i].vx;
		moonsOut[i].y += moonsOut[i].vy;
		moonsOut[i].z += moonsOut[i].vz;
	}
	return moonsOut;
}


int calcTotalEnergy(const std::vector<Moon>& moons)
{
	int tot=0;
	for (auto & moon : moons)
	{
	int ke = 0; int pe = 0;
		ke += abs(moon.x) + abs(moon.y) + abs(moon.z);
		pe += abs(moon.vx) + abs(moon.vy) + abs(moon.vz);
		tot += ke * pe;
	}
	return tot;
}

int main()
{
	auto problem = readFile("problem.txt");
	auto moons = createMoons(split(problem.c_str(), "\n"));

	int stepsTaken = 0;
	int64_t cycles[3] = { -1,-1,-1 };

	auto initMoons = moons;
	for (;;)
	{
		moons = step(moons);
		stepsTaken++;

		if (stepsTaken == 1000)
		{
			printf("Part A: %d\n", calcTotalEnergy(moons));
		}

		// If the moons are repeating along an axis we have something.
		bool stable[3] = { true, true, true };

		for (size_t i = 0; i < moons.size(); i++)
		{
			auto& moon = moons[i];
			auto& initMoon = initMoons[i];
			stable[0] = stable[0] && (initMoon.x == moon.x && initMoon.vx == moon.vx);
			stable[1] = stable[1] && (initMoon.y == moon.y && initMoon.vy == moon.vy);
			stable[2] = stable[2] && (initMoon.z == moon.z && initMoon.vz == moon.vz);
		}
		bool anyUnstable = false;
		for (int i = 0; i < 3; i++)
		{
			if (cycles[i] == -1)
			{
				if (stable[i])
					cycles[i] = stepsTaken;
				else
					anyUnstable = true;
			}
		}
		if (stepsTaken >= 1000 && !anyUnstable)
			break;
	}

	printf("Part B: %zd\n", lcm(cycles[0], lcm(cycles[1], cycles[2])));
	printf("Took %d steps\n", stepsTaken);
}