// day11.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <algorithm>
#include <iostream>
#include <string>
#include <vector>
#include <charconv>
#include <cassert>
#include <regex>
#include <fstream>
#include <map>

#include "../../tools.h"

using namespace std;

std::vector<int64_t> getCodes(const char* program)
{
	auto scodes = split(program, ",");

	std::vector<int64_t>codes;
	for (auto scode : scodes)
	{
		int64_t v;
		std::from_chars(scode.data(), scode.data() + scode.size(), v);
		codes.emplace_back(v);
	}
	return codes;
}

struct Computer
{
	Computer(const char* program)
	{
		_codes = getCodes(program);
	}

	Computer(const std::vector<int64_t>& program)
	{
		_codes = program;
	}

	void addInputs(std::initializer_list<int64_t> inputs)
	{
		_inputs.insert(_inputs.end(), inputs.begin(), inputs.end());
	}

	bool getOutput(int64_t& x)
	{
		if (_outputs.empty())
			return false;
		x = *_outputs.begin();
		_outputs.erase(_outputs.begin());
		return true;
	}

	bool halted()
	{
		return _halted;
	}

	void executeUntilOutOfInputs()
	{
		auto inputIt = _inputs.begin();
		for (;;)
		{
			_instructionsRan++;
			int64_t instrution = _codes[_offset];

			int64_t modes[3] = { (instrution / 100) % 10,
				 (instrution / 1000) % 10,
				(instrution / 10000) % 10 };
			instrution %= 100;

			auto readMode = [&](int off)
			{
				if (modes[off] == 1)
				{
					return  read(_offset + off + 1);
				}
				else if (modes[off] == 2)
				{
					return read(read(_offset + off + 1) + _relBase);
				}
				else
				{
					return read(read(_offset + off + 1));
				}
			};
			auto writeMode = [&](int off, int64_t value)
			{
				if (modes[off] == 1)
				{
					assert(0);
				}
				else if (modes[off] == 2)
				{
					return write(read(_offset + off + 1) + _relBase, value);
				}
				else
				{
					return write(read(_offset + off + 1), value);
				}
			};

			switch (instrution)
			{
			case 1:
				writeMode(2, readMode(0) + readMode(1));
				_offset += 4;
				break;
			case 2:
				writeMode(2, readMode(0) * readMode(1));
				_offset += 4;
				break;
			case 3:
			{
				if (_inputs.size())
				{
					writeMode(0, _inputs[0]);
					_inputs.erase(_inputs.begin());
					_offset += 2;
				}
				else
				{
					return;
				}
			}
			break;
			case 4:
				_outputs.emplace_back(readMode(0));
				_offset += 2;
				break;
			case 5:
				if (readMode(0) != 0)
				{
					_offset = readMode(1);
				}
				else
				{
					_offset += 3;
				}
				break;
			case 6:
				if (readMode(0) == 0)
				{
					_offset = readMode(1);
				}
				else
				{
					_offset += 3;
				}
				break;
			case 7:
				writeMode(2, (readMode(0) < readMode(1)) ? 1 : 0);
				_offset += 4;
				break;
			case 8:
				writeMode(2, (readMode(0) == readMode(1)) ? 1 : 0);
				_offset += 4;
				break;
			case 9:
				_relBase += readMode(0);
				_offset += 2;
				break;
			case 99:
				_halted = true;
				return;
			default:
				assert(0);
			}
		}
		assert(0);
	}
private:
	int64_t read(int64_t at)
	{
		expand(at);
		return _codes[at];
	}
	void write(int64_t at, int64_t v)
	{
		expand(at);
		_codes[at] = v;
	}
	void expand(int64_t at)
	{
		assert(at >= 0);
		if ((size_t)at >= _codes.size())
		{
			_codes.resize(at + 1);
		}
	}

	int64_t _instructionsRan = 0;
	int64_t _relBase = 0;
	std::vector<int64_t> _codes;
	size_t _offset = 0;
	bool _halted = false;
	std::vector<int64_t> _inputs;				// Current inputs.
	std::vector<int64_t> _outputs;
};

map<pair<int, int>, int64_t> runProgram(const std::vector<int64_t> &codes, int startColor)
{
	map<pair<int, int>, int64_t> colors;

	int dx = 0, dy = -1;
	int x = 0, y = 0;
	size_t s = 0;
	colors[make_pair(0, 0)] = startColor;
	Computer c(codes);
	while (!c.halted())
	{
		auto itor = colors.find(make_pair(x, y));
		int64_t color = 0;
		int64_t dir = 0;
		if (itor != colors.end())
			color = itor->second;

		c.addInputs({ color });
		c.executeUntilOutOfInputs();

		c.getOutput(color);

		colors[make_pair(x, y)] = color;

		c.getOutput(dir);

		int ndx, ndy;
		int sa = (dir == 0) ? -1 : 1;
		ndx = (int)(- sa * dy);
		ndy = (int)(sa * dx);
		dx = ndx;
		dy = ndy;
		x += dx;
		y += dy;
		if (s != colors.size())
		{
			s = colors.size();
		}
	}
	return colors;
}

int main()
{
	auto program = getCodes(readFile("problem.txt").c_str());

	printf("Part A: %zd\n", runProgram(program, 0).size());

	auto colors = runProgram(program, 1);

	int minx = std::numeric_limits<int>::max(), maxx = std::numeric_limits<int>::min();
	int miny = std::numeric_limits<int>::max(), maxy = std::numeric_limits<int>::min();
	for (const auto& color : colors)
	{
		minx = std::min(color.first.first, minx);
		maxx = std::max(color.first.first, maxx);
		miny = std::min(color.first.second, miny);
		maxy = std::max(color.first.second, maxy);
	}

	printf("Part B:\n");
	for (int y = miny; y <= maxy; y++)
	{
		for (int x = minx; x <= maxx; x++)
		{
			auto itor = colors.find(make_pair(x, y));
			int64_t color = 0;
			if (itor != colors.end())
				color = itor->second;
			printf("%s", color ? "*" : " ");
		}
		printf("\n");
	}
}