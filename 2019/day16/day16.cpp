// day16.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <algorithm>
#include <iostream>
#include <string>
#include <vector>
#include <charconv>
#include <cassert>
#include <regex>
#include <fstream>
#include <map>
#include <unordered_map>
#include <thread>
#include <set>
#include <numeric>

#include "../../tools.h"
#include "../../graph.h"

//
// Safe stack allocations that if they are too big go back to the heap.
//
// This optimization gives about a 30% boost. Heap is bad.
//
constexpr size_t MarkSize = 16;
constexpr int64_t kStackMark = 0x12345678ULL;
constexpr int64_t kHeapMark = 0x87654321ULL;
constexpr size_t kMaxHeapSize = 16*1024;

size_t ComputeSafeAllocASize(size_t y)
{
	return y + MarkSize;
}

void* MarkSafeAllocASize(void* ptr, bool stack)
{
	auto x = reinterpret_cast<int64_t*>(ptr);
	*x = stack ? kStackMark : kHeapMark;
	return reinterpret_cast<char*>(ptr) + MarkSize;
}

void* SafeMalloc(size_t size)
{
	return MarkSafeAllocASize(malloc(ComputeSafeAllocASize(size)), false);
}

void FreeSafeAllocASize(void* ptr)
{
	if (ptr)
	{
		ptr = reinterpret_cast<char*>(ptr) - MarkSize;
		int64_t* x = reinterpret_cast<int64_t*>(ptr);
		if (*x == kHeapMark)
		{
			free(ptr);
		}
	}
}

// There is a general distian for preprocessor macros, and it is well earned, but sometimes there is no other way.
#define SAFE_ALLOCA(x) ((ComputeSafeAllocASize(x) > kMaxHeapSize) ? SafeMalloc(x) : MarkSafeAllocASize(alloca(ComputeSafeAllocASize(x)), true))
#define SAFE_FREEA(ptr) FreeSafeAllocASize(ptr)

using namespace std;

int basePattern[] = { 0, 1, 0, -1 };

int getPattern(int phase, int off)
{
	int idx = (off + 1) / (phase+1);
	idx %= 4;
	if (idx < 0) idx += 4;
	return basePattern[idx];
}

std::vector<int> doitSlow(const std::vector<int> &input, int phases)
{
	std::vector<int> current = input;
	for (int phase = 0; phase < phases; phase++)
	{
		std::vector<int> next; next.resize(current.size());

		for (int j = 0; j < (int)current.size(); j++)
		{
			int sum = 0;
			for (int k = 0; k < (int)input.size(); k++)
			{
				sum += current[k] * getPattern(j, k);

			}

			sum = abs(sum % 10);
			next[j] = sum;
		}
		current = std::move(next);
	}
	return current;
}

int wrap(int x)
{
	return abs(x % 10);
}

// This is way over thinking the problem, but still an interesting thought... 
// With the optimization here in this it is O(n^1.5). But still really slow for this problem.
// 
// This was determined through analysis rather than doing this via something math. Also since each portion is independent
// of every other portion, you could scale this through threading.
//
void round(const int* input, int* output, int x, int y, int sx, int sy)
{
	if (sy == 0)
	{
		return;
	}
	if (sx == 0)
	{
		for (int i = 0; i < sy; i++)
		{
			output[i] = 0;
		}
		return;
	}
	if (sx == 1)
	{
		for (int i = 0; i < sy; i++)
		{
			output[i] = input[0]* getPattern(y+i, x);
		}
		return;
	}
	if (sy == 1)
	{
		int sum = 0;
		for (int i = 0; i < sx; i++)
		{
			sum += input[i] * getPattern(y, x + i);
		}
		output[0] = sum;
		return;
	}
	// This is the optimization opportunity that makes us O(n^1.5)
	{
		// The reason this works everything in the group is the same, this happens with a great deal of frequency if you pick any
		// square at random. The Matrix is a triangular matrix for one thing.
		int idx0 = (x + 1) / (y + 1);
		int idx1 = (x + sx) / (y + 1);
		bool allSame = idx0 == idx1;
		int idx = idx0 % 4;

		for (int yo = y; yo < y + sy && allSame; yo++)
		{
			int idx0 = (x + 1) / (yo + 1);
			int idx1 = (x + sx) / (yo + 1);
			if (!(idx0 == idx1 && (idx1 % 4) == idx))
				allSame = false;
		}
		if (allSame)
		{
			if (basePattern[idx] == 0)
			{
				for (int i = 0; i < sy; i++)
				{
					output[i] = 0;
				}
				return;
			}
			else
			{
				int sum = 0;
				for (int i = 0; i < sx; i++)
				{
					sum += input[i];
				}
				sum *= basePattern[idx];
				for (int i = 0; i < sy; i++)
				{
					output[i] = sum;
				}
				return;
			}
		}
	}

	int sx_1 = sx / 2;
	int sx_2 = sx - sx_1;
	int sy_1 = sy / 2;
	int sy_2 = sy - sy_1;
	int toAllocate = max(sy_1, sy_2);

	// Do the upper half first, then the lower half.
	// Now the hard part we have to create two vectors of half size.
	int* data = (int*)SAFE_ALLOCA(sizeof(int) * toAllocate * 2);
	int* left = data;
	int* right = left + toAllocate;

	round(input,	     left, x       , y, sx_1, sy_1);
	round(input + sx_1, right, x + sx_1, y, sx_2, sy_1);

	for (int j = 0; j < sy_1; j++)
	{
		output[j] = left[j] + right[j];
	}

	// Bottom half.
	round(input,         left, x       , y + sy_1, sx_1, sy_2);
	round(input + sx_1, right, x + sx_1, y + sy_1, sx_2, sy_2);

	for (int j = sy_1; j < sy; j++)
	{
		output[j] = left[j - sy_1] + right[j - sy_1];
	}
	SAFE_FREEA(data);
}

std::vector<int> doitFast(const std::vector<int>& input, int phases)
{
	std::vector<int> current = input;
	for (int phase = 0; phase < phases; phase++)
	{
		std::vector<int> next; next.resize(current.size());

		round(current.data(), next.data(), 0, 0, (int)current.size(), (int)current.size());

		for (auto& j : next)
		{
			j = wrap(j);
		}

		swap(current, next);
	}
	return current;
}

// This is much faster than the O(n^1.5) routine we found, this is O(n)
std::vector<int> handleLowerDiagnal(const std::vector<int>& input, int phases)
{
	std::vector<int> current = input;
	std::vector<int> next; next.resize(current.size());
	int sum = 0;
	for (auto x : current) sum += x;
	for (int phase = 0; phase < phases; phase++)
	{
		int sum2 = 0;
		for (int j = 0; j < (int)current.size(); j++)
		{
			next[j] = wrap(sum);
			sum2 += next[j];
			sum -= current[j];
		}
		sum = sum2;
		swap(current, next);
	}
	return current;
}

std::vector<int> read(const string_view &s)
{
	std::vector<int> v;
	v.reserve(s.size());
	for (auto c : s)
	{
		v.emplace_back(c - '0');
	}
	return v;
}

void print(const std::vector<int> &v, int size = -1)
{
	if (size == -1 || size > (int)v.size())
	{
		size = (int)v.size();
	}
	for(int i = 0; i < size; i++)
	{
		printf("%d", v[i]);
	}
	printf("\n");
}

// This function basically makes it so that you can access a vector beyond its natural range, this cleans up
// the doitFaster. Notably, negative values always return 0, and values beyond the end of the array return the
// last value in the array.
//
int clampedAccess(const std::vector<int>& data, int at)
{
	if (at < 0) return 0;
	if (at >= (int)data.size()) return data.back();
	return data[at];
}

// O(n log n)
//
// The reason is,the outer loop runs n times, but the inner loop runs like this,
// n, n/2, n/3, n/4, n/5... = n ( 1/1 + 1/2 + 1/3 + 1/4 + 1/5 + ... + 1/n)
// ( 1/1 + 1/2 + 1/3 + 1/4 + 1/5 + ... + 1/n) is the Harmonic Series which is known 
// to be O(lg n) so this expression is O(n log n)
std::vector<int> doitFaster(const std::vector<int>& input, int phases)
{
	vector<int> current = input;
	std::vector<int> partialSum(current.size());
	std::vector<int> next(current.size());
	for (int phase = 0; phase < phases; phase++)
	{
		partial_sum(current.begin(), current.end(), partialSum.begin());

		// Now that we have the cum sum use it to compute the values for each phase
		for (int i = 0; i < (int)input.size(); i++)
		{
			// Amount we skip each time.
			int skip = i + 1;
			int at = -2;		// -2 (One step is because the pattern is shifted once, the second is to deal with the pattern of the partial sum.
			int pattern = 0;
			int sum = 0;
			while(at < (int)input.size())
			{
				sum += (clampedAccess(partialSum, at + skip) - clampedAccess(partialSum, at)) * basePattern[pattern];
				pattern = (pattern + 1) % 4;
				at += skip;
			}
			next[i] = wrap(sum);
		}
		// Use a swap to avoid allocating more memory.
		swap(current, next);
	}
	return current;
}

int main()
{
	const char* example1 = "12345678";
	const char* example2 = "80871224585914546619083218645595";
	const char* example3 = "19617804207202209144916044189917";
	const char* example4 = "69317163492948606335995924319873";
	auto input = readFile("problem.txt");
	auto a = read(input.c_str());

	{
		auto values = read(example1);
		print(values);
		for (int i = 0; i < 4; i++)
		{
			values = doitFaster(values, 1);
			print(values);
		}
	}
	print(doitFaster(read(example2), 100), 8);
	print(doitFaster(read(example3), 100), 8);
	print(doitFaster(read(example4), 100), 8);

	printf("Part A: ");
	print(doitFaster(read(input.c_str()), 100), 8);

	// Repeat 10000, times
	const int repetitions = 10000;
	string partBString;
	partBString.reserve(input.size() * repetitions);
	for (int i = 0; i < repetitions; i++)
	{
		partBString += input;
	}
	auto partBVector = read(partBString.c_str());

	// Read first 8.
	int off = 0;
	for (int i = 0; i < 7; i++)
	{
		off = off * 10 + a[i];
	}
	partBVector = std::vector(partBVector.begin() + off, partBVector.end());
	printf("Part B: ");
	print(handleLowerDiagnal(partBVector, 100), 8);
}