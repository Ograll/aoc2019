// day15.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <algorithm>
#include <iostream>
#include <string>
#include <vector>
#include <charconv>
#include <cassert>
#include <regex>
#include <fstream>
#include <map>
#include <unordered_map>
#include <thread>
#include <set>

#include "../../tools.h"
#include "../../graph.h"

using namespace std;

std::vector<int64_t> getCodes(const char* program)
{
	auto scodes = split(program, ",");

	std::vector<int64_t>codes;
	for (auto scode : scodes)
	{
		int64_t v;
		std::from_chars(scode.data(), scode.data() + scode.size(), v);
		codes.emplace_back(v);
	}
	return codes;
}

struct Computer
{
	Computer(const char* program)
	{
		_codes = getCodes(program);
	}

	Computer(const std::vector<int64_t>& program)
	{
		_codes = program;
	}

	void addInputs(const vector<int>& inputs)
	{
		_inputs.insert(_inputs.end(), inputs.begin(), inputs.end());
	}

	bool hasOutputs()
	{
		return _outputs.size();
	}

	bool getOutput(int64_t& x)
	{
		if (_outputs.empty())
			return false;
		x = *_outputs.begin();
		_outputs.erase(_outputs.begin());
		return true;
	}

	bool halted()
	{
		return _halted;
	}

	void executeUntilOutOfInputs()
	{
		auto inputIt = _inputs.begin();
		for (;;)
		{
			_instructionsRan++;
			int64_t instrution = _codes[_offset];

			int64_t modes[3] = { (instrution / 100) % 10,
				 (instrution / 1000) % 10,
				(instrution / 10000) % 10 };
			instrution %= 100;

			auto readMode = [&](int off)
			{
				if (modes[off] == 1)
				{
					return  read(_offset + off + 1);
				}
				else if (modes[off] == 2)
				{
					return read(read(_offset + off + 1) + _relBase);
				}
				else
				{
					return read(read(_offset + off + 1));
				}
			};
			auto writeMode = [&](int off, int64_t value)
			{
				if (modes[off] == 1)
				{
					assert(0);
				}
				else if (modes[off] == 2)
				{
					return write(read(_offset + off + 1) + _relBase, value);
				}
				else
				{
					return write(read(_offset + off + 1), value);
				}
			};

			switch (instrution)
			{
			case 1:
				writeMode(2, readMode(0) + readMode(1));
				_offset += 4;
				break;
			case 2:
				writeMode(2, readMode(0) * readMode(1));
				_offset += 4;
				break;
			case 3:
			{
				if (_inputs.size())
				{
					writeMode(0, _inputs[0]);
					_inputs.erase(_inputs.begin());
					_offset += 2;
				}
				else
				{
					return;
				}
			}
			break;
			case 4:
				_outputs.emplace_back(readMode(0));
				_offset += 2;
				break;
			case 5:
				if (readMode(0) != 0)
				{
					_offset = readMode(1);
				}
				else
				{
					_offset += 3;
				}
				break;
			case 6:
				if (readMode(0) == 0)
				{
					_offset = readMode(1);
				}
				else
				{
					_offset += 3;
				}
				break;
			case 7:
				writeMode(2, (readMode(0) < readMode(1)) ? 1 : 0);
				_offset += 4;
				break;
			case 8:
				writeMode(2, (readMode(0) == readMode(1)) ? 1 : 0);
				_offset += 4;
				break;
			case 9:
				_relBase += readMode(0);
				_offset += 2;
				break;
			case 99:
				_halted = true;
				return;
			default:
				assert(0);
			}
		}
		assert(0);
	}
private:
	int64_t read(int64_t at)
	{
		expand(at);
		return _codes[at];
	}
	void write(int64_t at, int64_t v)
	{
		expand(at);
		_codes[at] = v;
	}
	void expand(int64_t at)
	{
		assert(at >= 0);
		if ((size_t)at >= _codes.size())
		{
			_codes.resize(at + 1);
		}
	}

	int64_t _instructionsRan = 0;
	int64_t _relBase = 0;
	std::vector<int64_t> _codes;
	size_t _offset = 0;
	bool _halted = false;
	std::vector<int64_t> _inputs;				// Current inputs.
	std::vector<int64_t> _outputs;
};

void inputToDirection(int dir, int& dx, int& dy)
{
	switch (dir)
	{
	case 1: dx = 0; dy = 1; return;
	case 2: dx = 0; dy = -1; return;
	case 3: dx = 1; dy = 0; return;
	case 4: dx = -1; dy = 0; return;
	}
}

// BFS on the intcode machine.
std::map<pair<int, int>, int> exploreWorld(const std::vector<int64_t>& code)
{
	struct State
	{
		std::shared_ptr<Computer> computer;	// The computer state before this action was taken.
		int move;							// The action to take.
		int moves;							// The number of actions taken.
		int x = 0, y = 0;					// The world position.
	};
	using QueueEntry = State;
	struct Comparer
	{
		bool operator()(const QueueEntry& a, QueueEntry& b) const
		{
			return a.moves > b.moves;
		}
	};
	using queue = std::priority_queue<QueueEntry, std::vector<QueueEntry>, Comparer>;
	queue frontier;
	frontier.emplace(State{ std::make_shared<Computer>(code), 0, 0, 0, 0 });

	std::map<pair<int, int>, int> explored;

	while (frontier.empty() == false)
	{
		QueueEntry item = frontier.top();
		frontier.pop();

		if (explored.find(pair(item.x, item.y)) != explored.end())
		{
			continue;
		}
		shared_ptr<Computer> program = item.computer;
		if (item.moves == 0)
		{
			explored[pair(item.x, item.y)] = 1;
		}
		else
		{
			program = shared_ptr<Computer>(new Computer(*program));

			program->addInputs({ item.move });
			program->executeUntilOutOfInputs();
			int64_t x;
			program->getOutput(x);
			assert(program->hasOutputs() == false);
			explored[pair(item.x, item.y)] = (int)x;
			if (x == 0)
			{
				continue;
			}
		}

		for (auto& input : { 1, 2, 3, 4 })
		{
			int dx, dy;
			inputToDirection(input, dx, dy);
			frontier.emplace(State{ program, input, item.moves + 1, item.x + dx, item.y + dy });
		}
	}
	return explored;
}

// BFS on the map, don't use the intcode anymore, its slow.
std::vector<int> findOxygen(const std::map<pair<int, int>, int> &map)
{
	struct Action
	{
		int move;
		Action* prevAction;
	};
	struct State
	{
		int x, y;
		int move;
		int moves;
		Action* prevAction;
	};
	using QueueEntry = State;
	struct Comparer
	{
		bool operator()(const QueueEntry& a, QueueEntry& b) const
		{
			return a.moves > b.moves;
		}
	};
	using queue = std::priority_queue<QueueEntry, std::vector<QueueEntry>, Comparer>;
	queue frontier;
	frontier.emplace(State{ });
	std::map<pair<int, int>, Action> explored;

	while (frontier.empty() == false)
	{
		QueueEntry item = frontier.top();
		frontier.pop();

		auto result = explored.emplace(pair(item.x, item.y), Action{ item.move, item.prevAction });
		if (result.second == false)
		{
			continue;
		}
		Action *prevAction = &result.first->second;

		int x = map.find(pair(item.x, item.y))->second;
		if (x == 0)
			continue;
		if (x == 2)
		{
			std::vector<int> moves;
			moves.resize(item.moves);
			auto action = prevAction;
			int i = item.moves;
			while (i--)
			{
				moves[i] = prevAction->move;
				prevAction = prevAction->prevAction;
			}
			return moves;
		}

		for (auto& input : { 1, 2, 3, 4 })
		{
			int dx, dy;
			inputToDirection(input, dx, dy);
			frontier.emplace(State{ item.x + dx, item.y + dy, input, item.moves+1, prevAction });
		}
	}
	return {};
}

int timeToFill(std::map<pair<int, int>, int> world)
{
	int minutes = 0;
	for (;;)
	{
		bool changed = false;
		auto newWorld = world;

		for (auto worldItor : world)
		{
			if (worldItor.second != 2)
			{
				continue;
			}

			// Scan each direction if there is an O spread it.
			for (int i = 1; i <= 4; i++)
			{
				int dx, dy;
				inputToDirection(i, dx, dy);

				auto newWorldItor = newWorld.find(pair(worldItor.first.first + dx, worldItor.first.second + dy));

				if (newWorldItor != newWorld.end() && newWorldItor->second == 1)
				{
					newWorldItor->second = 2;
					changed = true;
				}
			}
		}
		if (!changed)
			break;
		minutes++;
		world = std::move(newWorld);
	}
	return minutes;
}

int main()
{
	auto program = getCodes(readFile("problem.txt").c_str());

	auto world = exploreWorld(program);

	auto toOxygen = findOxygen(world);

	printf("Part A: %zu\n", toOxygen.size());
	printf("Part B: %d\n", timeToFill(world));
}