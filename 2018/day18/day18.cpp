// day5.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>
#include <vector>
#include <charconv>
#include <cassert>
#include <regex>
#include <fstream>
#include <map>
#include "../../tools.h"

using namespace std;

const char *example1 = R"(.#.#...|#.
.....#|##|
.|..|...#.
..|#.....#
#.#|||#|#|
...#.||...
.|....|...
||...#|.#|
|.||||..|.
...#.|..|.)";

class Grid
{
public:
	Grid(int width, int height) : width(width), height(height)
	{
		buffer = std::make_unique<char[]>(width * height);
	}

	int getWidth() const
	{
		return width;
	}

	int getHeight() const
	{
		return height;
	}

	void set(int x, int y, char c)
	{
		assert(x < width);
		assert(y < height);
		buffer[y * width + x] = c;
	}
	char get(int x, int y) const
	{
		assert(x < width);
		assert(y < height);
		return buffer[y * width + x];
	}

	int getSurround(int x, int y, int c) const
	{
		int s = 0;
		for (int dx = -1; dx <= 1; dx++)
		{
			for (int dy = -1; dy <= 1; dy++)
			{
				int tx = x + dx;
				int ty = y + dy;
				if (tx < 0 || ty < 0 || tx >= width || ty >= height || (dx == 0 && dy == 0))
					continue;
				if (get(tx, ty) == c)
					s++;
			}
		}
		return s;
	}

	void print()
	{
		for (int y = 0; y < getHeight(); y++)
		{
			for (int x = 0; x < getWidth(); x++)
			{
				printf("%c", get(x, y));
			}
			printf("\n");
		}
	}

	bool operator<(const Grid& grid) const
	{
		return lexicographical_compare(buffer.get(), buffer.get() + (width * height),
			grid.buffer.get(), grid.buffer.get() + (width * height));
	}

private:
	int width, height;
	std::unique_ptr<char[]> buffer;
};

Grid loadGrid(const char* grid)
{
	auto lines = split(grid, "\n");
	auto height = (int)lines.size();
	auto width = (int)lines[0].size();
	Grid g(width, height);

	for (int x = 0; x < width; x++)
	{
		for (int y = 0; y < height; y++)
		{
			g.set(x, y, lines[y][x]);
		}
	}
	return g;
}

Grid step(const Grid& grid)
{
	Grid newGrid(grid.getWidth(), grid.getHeight());
	for (int x = 0; x < grid.getWidth(); x++)
	{
		for (int y = 0; y < grid.getHeight(); y++)
		{
			switch (grid.get(x, y))
			{
			case '.':
				if (grid.getSurround(x, y, '|') >= 3)
					newGrid.set(x, y, '|');
				else
					newGrid.set(x, y, '.');
				break;
			case '|':
				if (grid.getSurround(x, y, '#') >= 3)
					newGrid.set(x, y, '#');
				else
					newGrid.set(x, y, '|');
				break;
			case '#':
				if (grid.getSurround(x, y, '#') < 1 || grid.getSurround(x, y, '|') < 1)
					newGrid.set(x, y, '.');
				else
					newGrid.set(x, y, '#');
				break;
			}
		}
	}
	return newGrid;
}

int resourceValue(const Grid& grid)
{
	int trees = 0, lumberyards = 0;
	for (int x = 0; x < grid.getWidth(); x++)
	{
		for (int y = 0; y < grid.getHeight(); y++)
		{
			switch (grid.get(x, y))
			{
			case '|': trees++; break;
			case '#': lumberyards++; break;
			}
		}
	}
	return trees * lumberyards;
}

int main()
{
	auto input = readFile("problem.txt");
	auto grid = loadGrid(example1);

	for (int i = 0; i < 10; i++)
	{
		grid = step(grid);
	}
	printf("Example A: %d\n", resourceValue(grid));

	grid = loadGrid(input.c_str());

	for (int i = 0; i < 10; i++)
	{
		grid = step(grid);
	}
	printf("Part A: %d\n", resourceValue(grid));

	std::map<Grid, int> cycleDetector;
	grid = loadGrid(input.c_str());

	double lastPercentDisplayed = 0;
	int cycleLength = 0;
	int i;
	for (i = 0; i < 1000000000; i++)
	{
		auto newGrid = step(grid);

		auto result = cycleDetector.emplace(std::piecewise_construct, 
			std::forward_as_tuple(std::move(grid)), 
			std::forward_as_tuple(i));

		if (result.second == false)
		{
			// We have detected the cycle.
			cycleLength = i - result.first->second;
			int skip = (1000000000 - i) / cycleLength;
			i += skip*cycleLength;
		}

		grid = std::move(newGrid);
	}
	printf("Part B: %d\n", resourceValue(grid));
}